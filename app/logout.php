<?php
session_start();
error_reporting(0);

// remove all session variables
session_unset(); 

// destroy the session 
session_destroy(); 

// Redirecting to index page
header('Location: ../view/index.php?msg=success');

?>