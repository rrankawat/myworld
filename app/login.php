<?php

session_start();

require_once('config.php');
require_once('functions.php');

if ($_SERVER["REQUEST_METHOD"] == "POST") {

	$email = test_input($_POST['email']);
	$password = test_input($_POST['password']);

	$sql = "SELECT id, name FROM users WHERE email='$email' AND password='$password'";
	$stmt = $conn->prepare($sql);
	$stmt->execute();

	if($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
		$_SESSION['id'] = $row['id'];
		$_SESSION['name'] = $row['name'];
		
		header('Location: ../view/index.php?msg=success');
	}
	else {
		header('Location: ../view/login.php?msg=error');
	}
}
else {
	header('Location: ../view/index.php');
}