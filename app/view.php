<?php

require_once('config.php');

$sql = "SELECT name FROM country ORDER BY name ASC"; 
$stmt = $conn->prepare($sql);
$stmt->execute();

$countries = array();
$count = 0;
while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
	$countries[] = $row;
}

/*
echo "<pre>";
print_r($countries);
*/