# myworld-task

The database table stores countries. Write on html, css, js, php interface to add a new country to the table and view the list of countries from the table. 

Create the table structure yourself. Deleting and editing is not necessary. 

A good solution is this: 

protection against injections when receiving parameters via get- or post; 
protection against dangerous text entered by the user; 
meaningful name of fields and database tables;
reasonable code design - the names of variables, functions, indents, comments; the code should be easy to maintain; 
no duplication of code; Kopipast - a sign of low culture development; 
startup in linux environments without dancing with tambourines (file name register, relative paths); 
separation of the logic of work and presentation - it is ugly to write html and php mixed; 
meaningful use of css and at least a little on js. 
HTML and CSS frameworks can be used. PHP code must be written from scratch. 

The criterion for the quality of the solution is security, reliability, and ease of implementation. 

Scope of improvement -->

1) database connection is not cheking (success or not)
2) you didn't used mysql_real_escape_string 
instead of this your tried to use
trim, stripslashes, htmlspecialchars . 
there are some possible obstacles in future
3) no comments, but there is commented code
4) We didn't ask for authorization, but why do you keep passwords as plain text?
5) An attempt to  View / Model approach detected, but there are many copied/pasted fragments. Only navbar is separated to a single block 
CSS and  JS are not separated, inline СSS  is bad practice
