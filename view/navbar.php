<nav class="navbar navbar-expand-md navbar-dark bg-dark">
  <a class="navbar-brand" href="index.php">Countries</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarsExampleDefault">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="index.php">Home</a>
      </li>
    </ul>
    <div class="form-inline my-2 my-lg-0">
      <?php if(isset($_SESSION['id'])) { ?>
        <a href="#" class="text-light text-lg"><?php echo $_SESSION['name']; ?></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="../app/logout.php" class="btn btn-success">Logout</a>
      <?php } else { ?>
        <a href="login.php" class="btn btn-success">Login</a>
      <?php } ?>
    </div>
  </div>
</nav>