<?php
session_start();

if(!isset($_SESSION['id'])) {

  session_unset(); 
  session_destroy();

  header('Location: index.php?jump=true');
}

?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Add New Country</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <!-- custom css -->
    <style type="text/css">
      #errorbox {
        display: none;
      }
    </style>
  </head>
  <body>
    <!-- Navigation Bar -->
    <?php require_once('navbar.php'); ?>

    <div class="container">

        <br /><a href="index.php" class="btn btn-secondary">Back</a>
        <br /><br /><h1>Add New Country</h1>
        
        <br />
        <!-- To display errors -->
        <div id="errorbox" class="alert alert-danger alert-dismissible fade show" role="alert">
          <span id="message"></span>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div>
            <form id="form" method="post" action="../app/addnew.php">
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="inputName">Country Name</label>
                  <input type="text" name="name" class="form-control" id="inputName">
                </div>
              </div>
              <br />
              <button type="button" id="button" class="btn btn-primary">Submit</button>
            </form>
        </div>

    </div><!-- Container -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

    <!-- Custom JavaScript -->
    <script type="text/javascript">

      function clearMessage(){
        errorbox.style.display = 'none';
        message.innerHTML = '';
      }

      //Form validation
      function validate() {
        var name = document.getElementById('inputName').value;

        if(name == "") {
          errorbox.style.display = 'block';
          message.innerHTML = 'Please enter <strong>Country</strong> name';
        }
        else {
          clearMessage();
          submitForm();
        }
      }

      function submitForm() {
        document.getElementById('form').submit();
      }
      
      var button = document.getElementById('button');
      button.addEventListener("click", validate);
    </script>
  </body>
</html>