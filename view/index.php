<?php
session_start();
error_reporting(0);

?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>Home Page</title>
  </head>
  <body>
    <!-- Navigation Bar -->
    <?php require_once('navbar.php'); ?>

    <div class="container">
        <br />
        <!-- To display errors -->
        <?php if($_GET['jump'] == true) { ?>
        <div id="errorbox" class="alert alert-danger alert-dismissible fade show" role="alert">
          <span id="message">Please login first</span>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <?php } ?>

        <!-- To display msg -->
        <?php if($_GET['msg'] == 'success') { ?>
        <div id="errorbox" class="alert alert-success alert-dismissible fade show" role="alert">
          <span id="message">Success</span>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <?php } ?>

        <h1>Countries</h1>
        <br /><a href="addnew.php" class="btn btn-primary">Add New</a>

        <!-- Receving Data -->
        <?php require_once('../app/view.php'); ?>
        <br /><br />
        <div>
          <table class="table table-hover" style="width: 70%;">
            <thead>
              <tr>
                <th scope="col" style="width: 50%;">#</th>
                <th scope="col" style="width: 50%;">Country</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach($countries as $country) { ?>
              <tr>
                <th scope="row"><?php echo ++$count; ?></th>
                <td><?php echo $country['name']; ?></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
    </div><!-- Container -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>